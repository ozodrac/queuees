## Queues

Controlador de filas, ideia macro, receber uma requisição e enviar esta mesma à certos endpoints,fazendo com que eles sejam assincronos e não travem conexões.


## Install
 - clone repo
 - cd src
 - composer install
 - npm install
 - php artisan migrate --seed
 - php artisan passport:install
 
## Para acessar os containers: docker-compose exec "container_name" "command"
 - docker-compose exec php bash (acesso ao bash no workdir)
 - docker-compose exec db bash ( acesso ao bash do container do banco, mysql -u root -p)

 # Auth
 - composer require laravel/ui --dev
 - php artisan ui vue --auth
 - composer require laravel/passport
 - php artisan migrate
 - php artisan passport:install

 # Broadcasting
 - composer require pusher/pusher-php-server
 - composer require predis/predis
 - npm install --save socket.io-client
 - npm install --save laravel-echo pusher-js
 
 # Horizon
 - composer require laravel/horizon
 - php artisan horizon:install
 - php artisan queue:failed-table
 - php artisan migrate
 - php artisan horizon:assets
 - php artisan horizon
 # Front End
 - composer require laravel/ui --dev
 # Jobs
 - php artisan make:job ProcessRequests

## Pos Install
 - docker-compose exec php php artisan key:generate
 - docker-compose exec php php artisan optimize
 - docker-compose exec php php horizon

 # Events
 >> Possivel se fazer automatico, adicionando os eventos em App\providers\EventServiceProviders se adiconar aos listeners o Evento=>[Listener]
 - php artisan make:event Nome
 - php artisan make:listener Nome





