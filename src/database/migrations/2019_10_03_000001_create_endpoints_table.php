<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEndpointsTable extends Migration
{
    private $table = 'endpoints';
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('secretaria_id')->unsigned();
            $table->string('name');
            $table->string('port')->nullable()->default(80);
            $table->string('api_key')->nullable();
            $table->string('api_secret')->nullable();
            $table->string('resource');
            $table->enum('verb',['POST','GET','PUT','DELETE'])->default('GET');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('secretaria_id')->references('id')->on('secretarias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
