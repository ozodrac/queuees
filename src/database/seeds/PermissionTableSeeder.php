<?php


use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class PermissionTableSeeder extends Seeder

{

    /**

     * Run the database seeds.

     *

     * @return void

     */

    public function run()

    {

        $permissions = [
            'audit-list',

            'permission-list',
            'permission-add',
            'permission-edit',
            'permission-del',

            'role-list',
            'role-add',
            'role-edit',
            'role-del',

            'user-list',
            'user-add',
            'user-edit',
            'user-del',

            'secretaria-list',
            'secretaria-add',
            'secretaria-edit',
            'secretaria-del',

            'endpoint-list',
            'endpoint-add',
            'endpoint-edit',
            'endpoint-del',

            'assitencia-social',
            'educacao',
            'fazenda',
            'recursos-humanos',
            'saude'
        ];
        $roles = [
            'Prefeito',
            'Secretario',
            'Diretor',
            'Coordenador',
            'Lider',
            'Agente de Atendimento'
        ];

        //auth()->user()->hasAnyPermission(['prefeito'])
        // auth()->user()->hasPermissionTo('prefeito')
        Role::firstOrCreate(['name' => 'Admin', 'guard_name' => 'web']);
        foreach ($permissions as $permission) {
            Permission::firstOrCreate(['name' => $permission]);
            // Permission::create(['name' => $permission]);
        }
        foreach ($roles as $role) {
            $r = Role::firstOrCreate(['name' => $role]);
            if($r->name == 'Prefeito') {
                $r->givePermissionTo('audit-list');
                $r->givePermissionTo('assitencia-social');
                $r->givePermissionTo('educacao');
                $r->givePermissionTo('fazenda');
                $r->givePermissionTo('recursos-humanos');
                $r->givePermissionTo('saude');
            }
        }

    }
}
