<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//AUTH
Auth::routes(['register' => false]);
Route::get('/profile', 'Profile\ProfileController@profile')->name('profile');
Route::get('/password', 'Profile\ProfileController@password')->name('password');
Route::get('/passport', 'PassportController@passport')->name('passport');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');


Route::post('/search', 'API\RequestsController@index')->name('search');

//Secretarias & Endpoints
Route::resource('/secretarias', 'SecretariaController');
Route::resource('/endpoints', 'API\EndpointsController');

//Roles, Permissions & Users
Route::resource('/roles', 'API\RoleController');
Route::resource('/permissions', 'API\PermissionController');
Route::resource('/users', 'API\UserController');

//Auditoria
Route::resource('/auditorias', 'AuditController');
