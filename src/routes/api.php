<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Route::middleware('api')->get('/search/{?cpf}','HomeController@search');

Route::post('/v1/login', 'API\UserController@login');

Route::group(['middleware' => 'auth:api'], function() {
    Route::group(['namespace'=>'Profile'],function(){
        Route::get('/profile/getAuthUser','ProfileController@getAuthUser');
        Route::put('/profile/updateAuthUser', 'ProfileController@updateAuthUser');
        Route::put('/profile/updateAuthUserPassword','ProfileController@updateAuthUserPassword');
    });
    Route::namespace('API')->group(function(){
        Route::get('/permissions', 'PermissionController@listPermission');
        Route::get('/roles', 'RoleController@listRoles');
        Route::get('/endpoints/{id?}', 'EndpointsController@list');
        Route::get('/search', 'RequestsController@doReqs');
    });
    Route::get('/secretarias', 'SecretariaController@list');
    Route::get('/users', 'API\UserController@list');
    Route::get('/auditorias', 'AuditController@list');
});

