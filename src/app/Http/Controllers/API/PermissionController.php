<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Session;

class PermissionController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('role:Admin');
        // $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        // $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }


    public function index() {
        return view('permissions.index');
    }
    public function listPermission(Request $request){
        $name = $request->get('filter');
        $sort = explode("|", $request->sort);
        $sort_field = $sort[0];
        $sort = $sort[1];

        $permissions = Permission::where('name', 'LIKE', '%' . $name . '%')
                     ->orderBy("{$sort_field}","{$sort}")->paginate(15);
        return $permissions;
    }

    public function create() {
        $roles = Role::get(); //Get all roles
        if(auth()->user()->can('permission-add')){
            return view('permissions.create')->with('roles', $roles);
        }
        return view('errors.403');
    }


    public function store(Request $request) {
        $this->validate($request, [
            'name'=>'required|max:40',
        ]);

        $name = $request['name'];
        $permission = new Permission();
        $permission->name = $name;

        $roles = $request['roles'];

        $permission->save();

        if (!empty($request['roles'])) { //If one or more role is selected
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record

                $permission = Permission::where('name', '=', $name)->first(); //Match input //permission to db record
                $r->givePermissionTo($permission);
            }
        }

        return redirect()->route('permissions.index')
            ->with('flash_message',
             'Permission'. $permission->name.' added!');

    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id) {
        return redirect('permissions');
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $permission = Permission::findOrFail($id);

        return view('permissions.edit', compact('permission'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id) {
        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:40',
        ]);
        $input = $request->all();
        $permission->fill($input)->save();

        return redirect()->route('permissions.index')
            ->with('flash_message',
             'Permission'. $permission->name.' updated!');

    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id) {
        $permission = Permission::findOrFail($id);
        //Make it impossible to delete this specific permission
        if ($permission->name == "Administer roles & permissions") {
            if (request()->ajax()) {
                return response()->json(['errors' => 'Cannot delete this Permission!']);
            }
            return redirect()->route('permissions.index')->with('success', 'Cannot delete this Permission!');
        }
        $permission->delete();
        if (request()->ajax()) {
            return response()->json(['success' => 'Permission deleted!']);
        }
        return redirect()->route('permissions.index')->with('success', 'Permission deleted!');
    }
}
