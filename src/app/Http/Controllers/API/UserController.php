<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    private $successStatus = 200;
    private $limit = 10;

    function __construct(){
        $this->middleware(['auth','role:Admin']);
    }

    public function index(){
        return view('users.index');
    }

    public function list(Request $request) {

        $name = $request->get('filter');
        $sort = explode("|", $request->sort);
        $sort_field = $sort[0];
        $sort = $sort[1];
        $users = User::where('name', 'LIKE', '%' . $name . '%')
            ->orderBy("{$sort_field}", "{$sort}")
            ->paginate(15);

        return $users;
    }

    public function create() {
        $roles = Role::get();
        return view('users.create', ['roles'=>$roles]);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6|confirmed'
        ]);

        $user = User::create($request->only('email', 'name', 'password'));

        $roles = $request['roles'];

        if (isset($roles)) {
            foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();
                $user->assignRole($role_r);
            }
        }

        return redirect()->route('users.index')
            ->with([
                'message' => 'Usuário criado com sucesso.',
                'message_class' => 'alert-success'
            ]);
    }

    public function show($id) {
        return redirect('users');
    }

    public function edit($id) {
        $user = User::findOrFail($id);
        $roles = Role::get();
        $permissions = Permission::get();

        // dd($user->roles()->pluck('name'), $user->permissions()->pluck('name'));
        return view('users.edit', compact('user', 'roles','permissions'));
    }

    public function update(Request $request, $id) {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users,email,'.$id,
            // 'password'=>'required|min:6|confirmed'
        ]);
        // if()
        $input = $request->only(['name', 'email']);
        if($request->password){
            $this->validate($request, [
                'password' => 'required|min:6|confirmed'
            ]);
            $input['password'] = $request->password;
        }
        $roles = $request['roles'];
        $permissions = $request['permissions'];

        $user->fill($input)->save();

        if (isset($roles)) {
            $user->roles()->sync($roles);
        }else {
            $user->roles()->detach();
        }

        if (isset($permissions)) {
            $user->permissions()->sync($permissions);
        }else {
            $user->permissions()->detach();
        }
        // dd($user->roles()->pluck('name'), $user->permissions()->pluck('name'));
        return redirect()->route('users.index')
            ->with([
                'message' => 'Usuário atualizado com sucesso.',
                'message_class' => 'alert-success'
            ]);
    }

    public function destroy($id) {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('users.index')
            ->with([
                'message' => 'Usuário excluído com sucesso.',
                'message_class' => 'alert-warning'
            ]);
    }

}

