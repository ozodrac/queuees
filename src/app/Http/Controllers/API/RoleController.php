<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;


class RoleController extends Controller
{

    function __construct(){
        $this->middleware(['auth','role:Admin']);
        // $this->middleware('permission:role-list');
        // $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        // $this->middleware('permission:role-delete', ['only' => ['destroy']]);
        // $this->middleware('role:Admin');
    }

    public function index(Request $request){
        // return view('roles.index');

        $name = $request->get('name');
        $roles = Role::where('name', 'LIKE', '%' . $name . '%')
            ->paginate(15);
        return view('roles.index')->with('roles', $roles);
    }

    public function listRoles(Request $request){
        $name = $request->get('filter');
        $sort = explode("|", $request->sort);
        $sort_field = $sort[0];
        $sort = $sort[1];
        $roles = Role::where('name', 'LIKE', '%' . $name . '%')
            ->orderBy("{$sort_field}", "{$sort}")
            ->paginate(15);
        foreach ($roles as $key => $role) {
            $roles[$key]->permissions = str_replace(array('[', ']', '"'), '', $role->permissions()->pluck('name'));
        }
        return $roles;
    }

    public function create(){
        $permissions = Permission::get();
        return view('roles.create', compact('permissions'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permissions' => 'required',
        ]);
        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permissions'));

        return redirect()->route('roles.index')->with('success', 'Role created successfully');
    }

    public function show($id){
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id", "=", "permissions.id")
            ->where("role_has_permissions.role_id", $id)
            ->get();
        return view('roles.show', compact('role', 'rolePermissions'));
    }

    public function edit($id){
        $role = Role::find($id);
        $permissions = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)
            ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
            ->all();
        return view('roles.edit', compact('role', 'permissions', 'rolePermissions'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name' => 'required',
        ]);

        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();

        $role->syncPermissions($request->input('permissions'));
        return redirect()->route('roles.index')
            ->with('success', 'Role updated successfully');
    }

    public function destroy($id){
        $role = Role::findOrFail($id);
        $role->delete();
        if (request()->ajax()) {
            return response()->json(['success' => 'Regra Removida!']);
        }
        return redirect()->route('roles.index')->with('success', 'Role deleted successfully');
    }
}
