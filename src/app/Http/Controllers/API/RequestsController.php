<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\RequestRecieved;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Jobs\ProcessRequests;
use App\Model\Endpoints;

class RequestsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        foreach(request()->post('orgao') as $key =>$id){
            $endpoint = Endpoints::findOrFail($id);
            $endpoints[$key] = $endpoint->only('name','port','api_key','api_secret','verb','resource');
        }

        event(new RequestRecieved(request()->all()));
        return view('search.retorno')->with(compact('endpoints'));

    }
    public function doReqs(Request $request){
        $dados = $request->all();
        // $retorno =  event(new RequestRecieved($request->all()) );
        // $retorno =  ProcessRequests::dispatch($dados);
        $client = new Client();
        foreach ($dados['orgaos'] as $orgao) {
            $url = $this->getRequestUrl($orgao);
            try {
                $response = $client->request('GET', "{$url}", ['query' => ['cpf' => $dados['cpf']]]);
                $data[$orgao] = json_decode($response->getBody()->getContents()); // 200
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                $data[$orgao]['status'] = $e->getResponse()->getStatusCode();
            }
        }
        return response()->json($data);
        // return response()->json($retorno);
    }
    public function getRequestUrl(String $endpoint){
        return "http://localhost:3000/$endpoint";
    }
}
