<?php

namespace App\Http\Controllers\API;

use App\Model\Endpoints;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EndpointsController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('endpoints.index');
    }
    public function list(Request $request){

        $name = $request->get('filter');
        $sort = explode("|", $request->sort);
        $sort_field = $sort[0];
        $sort = $sort[1];
        $secretarias = Endpoints::where('name', 'LIKE', '%' . $name . '%')
            ->orderBy("{$sort_field}", "{$sort}")
            ->paginate(15);

        return $secretarias;
    }

    public function create(){
        if (!auth()->user()->hasPermissionTo('admin')) {
            return redirect()->route('home')->with(['message' => 'Aceso Negado!.', 'message_class' => 'alert-warning']);
        }
        return view('endpoints.create');
    }


    public function store(Request $request)
    {
        //
    }


    public function show(Endpoints $endpoints)
    {
        //
    }

    public function edit(Endpoints $endpoints)
    {
        //
    }


    public function update(Request $request, Endpoints $endpoints)
    {
        //
    }


    public function destroy(Endpoints $endpoints)
    {
        //
    }
}
