<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PassportController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function passport()
    {
       return view('passport.passport');
    }

}
