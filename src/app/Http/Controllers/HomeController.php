<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\ProcessRequests;
use App\Events\RequestRecieved;
use App\Model\Secretaria;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $secretarias = Secretaria::all();
        return view('home',compact('secretarias'));
    }

    public function recebe(){
        $request = request();
        ProcessRequests::dispatch($request);
    }
}
