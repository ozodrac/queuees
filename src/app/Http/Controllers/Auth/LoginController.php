<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
// use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\User;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->guzzleClient = new Client(['base_uri' => env('PMA_API_BASE_URL')]);
        $this->token = $this->requestAccess();
    }
    public function login(Request $request)
    {
        $request->validate([
            'matricula' => 'required|numeric',
            'password' => 'required|string|min:8'
        ]);
        if (method_exists($this, 'hasTooManyLoginAttempts') && $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->checkAutorization($request) != 200) {
            return response()->json(['errors'=>'Usuário não autorizadao/inativo ,Credenciais Inválidas']);
        }

        if (Auth::attempt($request->only('matricula','password'))) {
            $this->makeToken($request);
            return $this->sendLoginResponse($request);
        }
        return $this->autoLogin($request);

    }

    public function makeToken(Request $request){
        $tokenResult = $request->user()->createToken("Web Access {$request->matricula} " . Carbon::now());
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(224);
        }
        $token->save();
        return $tokenResult->accessToken;
    }
    public function autoLogin(Request $request){
        try {
            $response = $this->guzzleClient->post("funcionarios", [
                'json' => ['token' => $this->token, "matricula" => $request->matricula]
            ]);
            $data =  json_decode($response->getBody()->getContents())[0];
            $user = User::where('matricula', $request->matricula)->first();
            if(!$user){
                $user = User::create([
                    'name' => $data->nome,
                    'email' => $data->email,
                    'cargo' => $data->cod_cargo,
                    'desc_cargo' => $data->cargo,
                    'cpf' => $data->cpf,
                    'matricula' => $data->matricula,
                    'password' => Hash::make($request->password),
                ]);
                if($user->cargo==792){
                    $user->assignRole("Admin");
                }else{
                    $user->assignRole("Agente de Atendimento");
                }
            }
            if (Hash::check($request->password, $user->password)) {
                Auth::attempt($request->only('matricula', 'password'));
                $this->makeToken($request);
                return $this->sendLoginResponse($request);
            }
            // echo $response;
            return response()->json(['errors' => 'Senha Inválida']);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return response()->json(['errors'=>'Usuário não encontrado']);
        }
    }
    public function checkAutorization(Request $request){
        try {
            $response = $this->guzzleClient->post("funcionarios",[
                'json' => ['token' => $this->token , "matricula" => $request->matricula ]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse()->getStatusCode();
        }
        return $response->getStatusCode();
    }

    public function requestAccess(){

        try {
            $response = $this->guzzleClient->post("login", [
                'json' => ['login' => env('PMA_API_KEY'), 'senha' => env('PMA_API_SECRET')]
            ]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return null;
        }

        return json_decode($response->getBody()->getContents());
    }

    public function logout(Request $request){
        foreach($request->user()->tokens()->get() as $token){
            $token->revoke();
        }
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }
}
