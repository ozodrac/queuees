<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AuditSearch;

class AuditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role_or_permission:Admin|audit-list');
        // $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
    }

    public function index()
    {
        return view('auditorias.index');
    }

    public function list(Request $request) {

        $name = $request->get('filter');
        $sort = explode("|", $request->sort);
        $sort_field = $sort[0];
        $sort = $sort[1];

        $auditorias = AuditSearch::select('consultas_realizadas.*','users.name')
            ->join('users', 'users.id', '=', 'consultas_realizadas.user_id')
            ->where('consultas_realizadas.termo_pesquisado', 'LIKE', '%' . $name . '%')
            ->orderBy("{$sort_field}", "{$sort}")
            ->paginate(15);

        return $auditorias;
    }
}
