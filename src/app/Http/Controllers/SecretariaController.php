<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Secretaria;
use App\Model\Endpoints;

class SecretariaController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(){
        return view('secretarias.index');
    }

    public function list(Request $request) {

        $name = $request->get('filter');
        $sort = explode("|", $request->sort);
        $sort_field = $sort[0];
        $sort = $sort[1];
        $secretarias = Secretaria::where('name', 'LIKE', '%' . $name . '%')
            ->orderBy("{$sort_field}", "{$sort}")
            ->paginate(15);

        return $secretarias;
    }

    public function create(){
        $metodos = ['GET' => 'GET', 'POST' => 'POST'];
        return view('secretarias.create', compact('metodos'));
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'descricao' => 'required',
        ]);
        $secretaria = Secretaria::create($request->only('name', 'descricao', 'sigla'));
        if($request->base_url){
            $create = ['secretaria_id'=>$secretaria->id,
                        'name'=>$request->base_url,
                        'port'=>$request->port,
                        'api_key'=>$request->api_key,
                        'api_secret'=>$request->api_secret,
                        'verb'=>$request->verb,
                        'resource'=>$request->recurso
                    ];
            $secretaria->endpoint = Endpoints::create($create);
        }
        return redirect()->route('secretarias.index')
            ->with([
                'message' => 'Cadastro efetuado com sucesso.',
                'message_class' => 'alert-success'
            ]);
        // return redirect('secretarias')->with('success', 'Cadastro efetuado com sucesso!');
    }

    public function show($id)
    {
        //
    }

    public function edit($id){
        $secretaria = Secretaria::find($id);
        $metodos = ['GET'=>'GET','POST'=>'POST'];
        // dd($secretaria->endpoints()->get());
        return view('secretarias.edit', compact('secretaria','metodos'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'name' => 'required',
            'descricao' => 'required',
        ]);
        $secretaria = Secretaria::findOrFail($id);
        $secretaria->update($request->only('name', 'descricao', 'sigla'));

        $update = [
            'secretaria_id' => $secretaria->id,
            'name' => $request->base_url,
            'port' => $request->port,
            'api_key' => $request->api_key,
            'api_secret' => $request->api_secret,
            'verb' => $request->verb,
            'resource' => $request->recurso
        ];
        if($request->base_url){
            $secretaria->endpoints()->first()->update($update);
        }
        return redirect()->route('secretarias.index')
            ->with([
                'message' => 'Secretaria atualizada com sucesso.',
                'message_class' => 'alert-success'
            ]);
        // if (request()->ajax()) {
        //     return response()->json(['success' => 'Secretaria atualizada com sucesso!']);
        // }
        // return redirect()->route('secretarias.index')->with('success', 'Secretaria atualizada com sucesso!');
    }

    public function destroy($id){
        Secretaria::where('id', $id)->delete();
        if (request()->ajax()) {
            return response()->json(['success' => 'Secretaria excluída com sucesso.']);
        }
        return redirect()->route('secretarias.index')->with('success', 'Secretaria excluída com sucesso.');
    }
}
