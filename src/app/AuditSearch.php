<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditSearch extends Model
{
    protected $table = 'consultas_realizadas';
    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'termo_pesquisado', 'orgaos_pesquisados' ];
    protected $dates = ['created_at'];
    function secretaria()
    {
        return $this->belongsTo(Secretaria::class);
    }
}
