<?php

namespace App\Listeners;

use App\Events\RequestRecieved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
// use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;

class HandleRequestRecieved implements ShouldQueue
{
    use InteractsWithQueue;

    public $connection = 'redis';
    public $queue = 'processamento';
    // public $delay = 5;
    // public $retorno;

    public function __construct(Request $event)
    {
        //
    }


    public function handle(RequestRecieved $event)
    {
        return $this->processRequest($event->request);
    }


    public function shouldQueue(RequestRecieved $event)
    {
        return $this->processRequest($event->request);
    }
    public function processRequest($dados){

        // $client = new Client();
        // foreach ($dados['orgaos'] as $orgao) {
        //     $url = $this->getRequestUrl($orgao);
        //     try {
        //         $response = $client->request('GET',"{$url}",[ 'query' => ['cpf' => $dados['cpf']] ] );
        //         $data[$orgao]= $response->getBody()->getContents(); // 200
        //     } catch (\GuzzleHttp\Exception\ClientException $e) {
        //         $data[$orgao]['status'] = $e->getResponse()->getStatusCode();
        //     }
        // }
        // // dd(response()->json($data));
        // return response()->json($data) ;
        return true;

    }
    public function getRequestUrl(String $endpoint){
        return "http://localhost:3000/$endpoint";
    }

    /**
     * Handle a job failure.
     *
     * @param  \App\Events\OrderShipped  $event
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(OrderShipped $event, $exception)
    {
        dump($event,$exception);
    }
}
