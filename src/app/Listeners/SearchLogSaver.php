<?php

namespace App\Listeners;

use App\Events\RequestRecieved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\AuditSearch;

class SearchLogSaver implements ShouldQueue
{
    use InteractsWithQueue;

    public $connection = 'redis';
    public $queue = 'processamento';
    public $delay = 25;
    // public $retorno;

    public function __construct(Request $event)
    {
    }

    public function handle(RequestRecieved $event){

    }

    public function shouldQueue(RequestRecieved $event){

        return $this->processRequest($event->request);
    }
    public function processRequest($dados)
    {
        $orgaos = implode(', ', array_keys($dados['orgao']));
        return AuditSearch::create([
            'user_id' => request()->user()->id,
            'termo_pesquisado' => $dados['cpf'],
            'orgaos_pesquisados' => $orgaos
        ]);
        // return true;
    }


    /**
     * Handle a job failure.
     *
     * @param  \App\Events\OrderShipped  $event
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(OrderShipped $event, $exception)
    {
        dump($event, $exception);
    }
}
