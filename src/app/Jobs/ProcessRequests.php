<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ProcessRequests implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public $requisicao;
    public $data;

    public function __construct()
    {

    //     $client = new Client();
    //     foreach ($request['orgaos'] as $orgao) {
    //         $url = "http://localhost:3000/{$orgao}";
    //         try {
    //             $response = $client->request('GET',"{$url}",[ 'query' => ['cpf' => $request['cpf']] ] );
    //             $this->data[$orgao]= $response->getBody()->getContents(); // 200
    //         } catch (\GuzzleHttp\Exception\ClientException $e) {
    //             $this->data[$orgao]['status'] = $e->getResponse()->getStatusCode();
    //         }
    //     }
    //     return response()->json($this->data);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Array $request)
    {
        $this->requisicao = $request;
        $client = new Client();
        foreach ($this->requisicao['orgaos'] as $orgao) {
            $url = "http://localhost:3000/{$orgao}";
            try {
                $response = $client->request('GET',"{$url}",[ 'query' => ['cpf' => $this->requisicao['cpf']] ] );
                $this->data[$orgao]= $response->getBody()->getContents(); // 200
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                $this->data[$orgao]['status'] = $e->getResponse()->getStatusCode();
            }
        }
        return response()->json($this->data);
    }
}
