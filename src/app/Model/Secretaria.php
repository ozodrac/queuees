<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Secretaria extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','descricao','sigla'];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($secretaria) {
            foreach ($secretaria->endpoints()->get() as $endpoint) {
                $endpoint->delete();
            }
        });
    }
    public function endpoints()
    {
        return $this->hasMany(Endpoints::class);
    }
}
