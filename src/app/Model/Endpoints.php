<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Endpoints extends Model
{
    use SoftDeletes;
    protected $table = 'endpoints';
    protected $primaryKey = 'id';

    // name
    // url
    // auth
    // content
    // headers
    // descricao
    protected $fillable = ['name', 'resource', 'secretaria_id','port', 'verb', 'api_key', 'api_secret'];
    protected $hidden = ['api_key','api_secret'];
    function secretaria()
    {
        return $this->belongsTo(Secretaria::class);
    }
}
