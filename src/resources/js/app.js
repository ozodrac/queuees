/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.$ = window.jQuery = require('jquery');
window.Vue = require('vue');
window.Swal = require('sweetalert2');
window.mask = require('jquery-mask-plugin');
window.validarCpf = require('lab-cpf-validator');

/*****
* CONFIGURATION
*/
//Main navigation
$.navigation = $('nav > ul.nav');
$.panelIconOpened = 'icon-arrow-up';
$.panelIconClosed = 'icon-arrow-down';

//Default colours
$.brandPrimary = '#20a8d8';
$.brandSuccess = '#4dbd74';
$.brandInfo = '#63c2de';
$.brandWarning = '#f8cb00';
$.brandDanger = '#f86c6b';

$.grayDark = '#2a2c36';
$.gray = '#55595c';
$.grayLight = '#818a91';
$.grayLighter = '#d1d4d7';
$.grayLightest = '#f8f9fa';

'use strict';

/****
* MAIN NAVIGATION
*/
/****
* CONFIRM ACTIONS
*/
$(document).on('click', '.confirm-action', function (e) {
    e.preventDefault();
    Swal.fire({
        title: "Confirmação",
        text: "Deseja realmente executar esta operação?",
        icon: "warning",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Cancelar",
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Remover!',
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete.value)
            $(this).closest('form')[0].submit();
    });



});

$(document).ready(function () {

    $('input[name=cpf]').mask('000.000.000-00', { reverse: true });

    $(`.btn-success[type=submit]`).attr({ disabled: true });
    
    $('input[name=cpf]').on('keyup', function (e) {
        let msg = '';
        let cpf = $(this).val().replace('.', '').replace('.', '').replace('-', '');
        if (cpf.length == 11) {
            msg = validarCpf(cpf) ? '' : `CPF ${$(this).val()} inválido`;
            if(msg.length){
                $(this).val('')
            }else{
                $(`.btn-success[type=submit]`).attr({ disabled: false });
            }
        }
        $('.resposta').html(msg).css("color","red");
    });

    if ($(`.switch-input[name^='orgao[']:checked`).length === 0) {
        $(`.btn-success[type=submit]`).attr({ disabled: true });
    } else {
        $(`.btn-success[type=submit]`).attr({ disabled: false });
    }
});

$(document).on('click', '.switch', function () {
    if ($(`.switch-input[name^='orgao[']:checked`).length === 0) {
        $(`.btn-success[type=submit]`).attr({ disabled: true });
    } else {
        $(`.btn-success[type=submit]`).attr({ disabled: false });
    }
})



//toast
import Toasted from 'vue-toasted'; Vue.use(Toasted)
Vue.toasted.register('successTop', message => message, { type: 'success', position: 'top-right', duration: 2000, iconPack: 'fontawesome', icon: 'check-circle' })
Vue.toasted.register('errorTop', message => message, { type: 'error', position: 'top-right', duration: 2000, iconPack: 'fontawesome', icon: 'times-circle' })

Vue.toasted.register('login', (message) => message, { type: 'success', position: 'bottom-center', duration: 1000, iconPack: 'fontawesome', icon: 'fingerprint' })

Vue.toasted.register('success', (message) => message, { type: 'success', position: 'bottom-center', duration: 1000 })
Vue.toasted.register('error', message => message, { type: 'error', position: 'bottom-center', duration: 1000 })
Vue.toasted.register('warning', message => message, { type: 'info', position: 'bottom-center', duration: 1000 })

// laravel-vue-datatable
import VueEvents from 'vue-events'
Vue.use(VueEvents);

window.Vuetable = require('vuetable-2');
Vue.use(Vuetable)
Vue.component("vuetable", Vuetable.default);
Vue.component("vuetable-pagination", Vuetable.VuetablePagination);
Vue.component("vuetable-pagination-info", Vuetable.VuetablePaginationInfo);
Vue.component('filter-bar', require('./components/tables/FilterComponent.vue').default)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('profile-component', require('./components/profile/ProfileComponent.vue').default);
Vue.component('password-component', require('./components/profile/PasswordComponent.vue').default);

//Retornos
Vue.component('retorno-component', require('./components/RetornoComponent.vue').default);
Vue.component('dadosbasicos-component', require('./components/retornos/DadosBasicosComponent.vue').default);
Vue.component('address-component', require('./components/retornos/AddressComponent.vue').default);
Vue.component('contacts-component', require('./components/retornos/ContactsComponent.vue').default);
Vue.component('vinculos-component', require('./components/retornos/VinculosComponent.vue').default);
Vue.component('folhas-vinculo-component', require('./components/retornos/FolhasVinculoComponent.vue').default);
Vue.component('pericias-vinculo-component', require('./components/retornos/PericiasVinculosComponent.vue').default);

//secretarias
Vue.component('secretarias-component', require('./components/secretarias/SecretariasComponent.vue').default);
Vue.component('endpoints-component', require('./components/secretarias/EndpointsComponent.vue').default);
Vue.component('endpoint-adder-component', require('./components/secretarias/EndpointAddComponent.vue').default);
Vue.component('button-addpoint', require('./components/secretarias/ButtonAddComponent.vue').default);

//login
Vue.component('login-component', require('./components/login/LoginComponent.vue').default);

// Roles
Vue.component('roles-component', require('./components/roles/RolesComponent.vue').default);

// Permissions
Vue.component('premissions-component', require('./components/permissions/PermissionsComponent.vue').default);

//passport
Vue.component('passport-clients', require('./components/passport/Clients.vue').default);
Vue.component('passport-authorized-clients', require('./components/passport/AuthorizedClients.vue').default);
Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue').default);

//User
Vue.component('users-component', require('./components/User/UsersComponent.vue').default);

//Auditoria
Vue.component('auditorias-component', require('./components/auditorias/AuditComponent.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
