@extends('layouts.app')

@section('title', '| Regras do sistema')

@section('content')

@if(session('message'))
<div class="alert {{ session('message_class', 'alert-info') }} alert-dismissible fade show" role="alert">
  {{ session('message') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="row" id="app">
    <auditorias-component></auditorias-component>
</div>
@endsection
