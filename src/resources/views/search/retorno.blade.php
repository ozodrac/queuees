@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12" id="app">

            <retorno-component
            v-bind:request={!!json_encode(request()->post())!!}
            v-bind:endpoints={!!json_encode($endpoints)!!}
            v-bind:roles={!! json_encode(auth()->user()->getRoleNames()) !!}
            v-bind:permissions={!! json_encode(auth()->user()->getPermissionNames()) !!}
            ></retorno-component>
            {{-- <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div> --}}
        </div>
    </div>
</div>
@endsection
