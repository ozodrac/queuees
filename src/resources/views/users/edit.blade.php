@extends('layouts.app')

@section('title', '| Editar usuário')

@section('content')


@if(session('message'))
<div class="alert {{ session('message_class', 'alert-info') }} alert-dismissible fade show" role="alert">
  {{ session('message') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if ($errors->any() && false)
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
@endif

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
              <i class='fa fa-address-card'></i> Atualizar usuário
            </div>

            <div class="card-body">
                    {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}

                <div class="row">

                    <div class="form-group col-sm-2">
                        {{ Form::label('matricula', 'Matricula',['class'=>'font-weight-bold']) }}
                        {{ Form::text('matricula', null, array('class' => 'form-control','readonly')) }}
                        <span class="help-block text-danger">{{ $errors->first('matricula') }}</span>
                    </div>

                    <div class="form-group col-sm-4">
                        {{ Form::label('desc_cargo', 'Cargo',['class'=>'font-weight-bold']) }}
                        {{ Form::email('desc_cargo', null, array('class' => 'form-control','readonly')) }}
                        <span class="help-block text-danger">{{ $errors->first('desc_cargo') }}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-4">
                        {{ Form::label('name', 'Nome',['class'=>'font-weight-bold']) }}
                        {{ Form::text('name', null, array('class' => 'form-control','readonly')) }}
                        <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                    </div>
                    <div class="form-group col-sm-3">
                        {{ Form::label('cpf', 'CPF',['class'=>'font-weight-bold']) }}
                        {{ Form::text('cpf', null, array('class' => 'form-control','readonly')) }}
                        <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                    </div>

                    <div class="form-group col-sm-4">
                        {{ Form::label('email', 'Email',['class'=>'font-weight-bold']) }}
                        {{ Form::email('email', null, array('class' => 'form-control')) }}
                        <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-4">
                        {{ Form::label('password', 'Senha',['class'=>'font-weight-bold']) }}<br>
                        {{ Form::password('password', array('class' => 'form-control')) }}
                        <span class="help-block text-danger">{{ $errors->first('password') }}</span>
                    </div>

                    <div class="form-group col-sm-4">
                        {{ Form::label('password', 'Confirmar senha',['class'=>'font-weight-bold']) }}<br>
                        {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
                        <span class="help-block text-danger">{{ $errors->first('password_confirmation') }}</span>
                    </div>
                </div>

                <div class="row">
                    {{ Form::label('roles', 'Perfis do usuário',['class'=>'font-weight-bold col-md-12']) }}
                    @foreach ($roles as $role)
                        <div class="col-sm-2">
                            {{ Form::checkbox('roles[]',  $role->id, $user->roles ) }}
                            {{ Form::label($role->name, ucfirst($role->name)) }}<br>
                        </div>
                    @endforeach
                    <span class="help-block text-danger">{{ $errors->first('roles') }}</span>
                </div>

                <div class="row">
                    {{ Form::label('roles', 'Permissões do usuário',['class'=>'font-weight-bold col-md-12']) }}
                    @foreach ($permissions as $permission)
                        <div class="col-sm-2">
                            {{ Form::checkbox('permissions[]',  $permission->id, $user->permission ) }}
                            {{ Form::label($permission->name, ucfirst($permission->name)) }}<br>
                        </div>
                    @endforeach
                    <span class="help-block text-danger">{{ $errors->first('roles') }}</span>
                </div>

                {{ Form::submit('Atualizar', array('class' => 'btn btn-primary')) }}

                <a class="btn btn-danger" href="{{ route('users.index') }}">Voltar</a>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@endsection
