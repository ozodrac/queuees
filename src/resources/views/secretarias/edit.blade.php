@extends('layouts.app')

@section('title', '| Adicionar nova regra.')

@section('content')


@if(session('message'))
<div class="alert {{ session('message_class', 'alert-info') }} alert-dismissible fade show" role="alert">
  {{ session('message') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
@endif

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                    <i class="fa fa-edit"></i> Editar Secretaria
            </div>
            <div class="card-body">
                {{ Form::model($secretaria, array('route' => array('secretarias.update', $secretaria->id), 'method' => 'PUT')) }}
                <div class="row">
                    <div class="form-group col-sm-6">
                        {{ Form::label('name', 'Nome*',['class'=>'font-weight-bold']) }}
                        {{ Form::text('name', $secretaria->name, ['class' => 'form-control',"placeholder"=>'Nome da Secretaria','required']) }}
                    </div>
                    <div class="form-group col-sm-4">
                        {{ Form::label('sigla*', 'Sigla',['class'=>'font-weight-bold']) }}
                        {{ Form::text('sigla', $secretaria->sigla, ['class' => 'form-control','required']) }}
                    </div>
                </div>
                @foreach ($secretaria->endpoints()->get() as $endpoint)
                    <div class="row">
                        <input type="hidden" name="endpoint" value="{{$endpoint->id}}">
                        <div class="form-group col-sm-5">
                        {{ Form::label('base_url', 'Url Base',['class'=>'font-weight-bold']) }}
                        {{ Form::text('base_url', $endpoint->name, array('class' => 'form-control','placeholder'=>'http://localhost')) }}
                        </div>
                        <div class="form-group col-sm-3">
                        {{ Form::label('port', 'Porta',['class'=>'font-weight-bold']) }}
                        {{ Form::text('port', $endpoint->port, array('class' => 'form-control','placeholder'=>'8080')) }}
                        </div>
                        <div class="form-group col-sm-4">
                        {{ Form::label('recurso', 'Recurso',['class'=>'font-weight-bold']) }}
                        {{ Form::text('recurso', $endpoint->resource, array('class' => 'form-control','placeholder'=>'/api/v1/secretaria')) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-5">
                        {{ Form::label('api_key', 'Chave da Api',['class'=>'font-weight-bold']) }}
                        {{ Form::text('api_key', $endpoint->api_key, array('class' => 'form-control')) }}
                        </div>
                        <div class="form-group col-sm-5">
                        {{ Form::label('api_secret', 'Senha da Api',['class'=>'font-weight-bold']) }}
                        {{ Form::text('api_secret', $endpoint->api_secret, array('class' => 'form-control')) }}
                        </div>
                        <div class="form-group col-sm-2">
                        {{ Form::label('verb', 'Método',['class'=>'font-weight-bold']) }}
                        {{ Form::select('verb', $metodos, $endpoint->verb, ['class'=>'form-control']) }}
                        </div>
                    </div>
                @endforeach
                <div class="row">
                    <div class="form-group col-sm-12">
                        {{ Form::label('descricao', 'Informações Adicionais') }}
                        {{ Form::textarea('descricao', $secretaria->descricao, ['class' => 'form-control',"placeholder"=>'Informações Adicionais']) }}
                    </div>
                </div>

                {{ Form::submit('Atualizar', array('class' => 'btn btn-primary')) }}

                <a class="btn btn-danger" href="{{ route('secretarias.index') }}">Voltar</a>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@endsection
