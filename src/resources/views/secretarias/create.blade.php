@extends('layouts.app')

@section('title', '| Adicionar nova regra.')

@section('content')


@if(session('message'))
<div class="alert {{ session('message_class', 'alert-info') }} alert-dismissible fade show" role="alert">
  {{ session('message') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
@endif

<div class="row" id='app'>
    <div class="col-lg-12">
                {{--  <button-addpoint class="float-right" style="position:relative;top:40%"></button-addpoint>  --}}
        <div class="card">
            <div class="card-header">
                    <i class='fa fa-plus'></i> Adicionar Secretaria
            </div>

            <div class="card-body">
                {{ Form::open(array('url' => '/secretarias')) }}

                <div class="row">
                    <div class="form-group col-sm-6">
                        {{ Form::label('name', 'Nome',['class'=>'font-weight-bold']) }}
                        {{ Form::text('name', null, ['class' => 'form-control',"placeholder"=>'Nome da Secretaria','required']) }}
                    </div>
                    <div class="form-group col-sm-4">
                        {{ Form::label('sigla', 'Sigla',['class'=>'font-weight-bold']) }}
                        {{ Form::text('sigla', null, ['class' => 'form-control','required']) }}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-5">
                      {{ Form::label('base_url', 'Url Base',['class'=>'font-weight-bold']) }}
                      {{ Form::text('base_url', '', array('class' => 'form-control','placeholder'=>'http://localhost')) }}
                    </div>
                    <div class="form-group col-sm-3">
                      {{ Form::label('port', 'Porta',['class'=>'font-weight-bold']) }}
                      {{ Form::text('port', '', array('class' => 'form-control','placeholder'=>'8080')) }}
                    </div>
                    <div class="form-group col-sm-4">
                      {{ Form::label('recurso', 'Recurso',['class'=>'font-weight-bold']) }}
                      {{ Form::text('recurso', '', array('class' => 'form-control','placeholder'=>'/api/v1/secretaria')) }}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-5">
                      {{ Form::label('api_key', 'Chave da Api',['class'=>'font-weight-bold']) }}
                      {{ Form::text('api_key', '', array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group col-sm-5">
                      {{ Form::label('api_secret', 'Senha da Api',['class'=>'font-weight-bold']) }}
                      {{ Form::text('api_secret', '', array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group col-sm-2">
                        {{ Form::label('verb', 'Método',['class'=>'font-weight-bold']) }}
                        {{ Form::select('verb', $metodos, null, ['class'=>'form-control']) }}
                    </div>
                </div>
                {{--  <endpoint-adder-component></endpoint-adder-component>  --}}
                <div class="row">
                    <div class="form-group col-sm-12">
                        {{ Form::label('descricao', 'Informações Adicionais') }}
                        {{ Form::textarea('descricao', null, ['class' => 'form-control',"placeholder"=>'Informações Adicionais']) }}
                    </div>
                </div>

                {{ Form::submit('Criar', array('class' => 'btn btn-primary')) }}

                <a class="btn btn-danger" href="{{ route('secretarias.index') }}">Voltar</a>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@endsection
