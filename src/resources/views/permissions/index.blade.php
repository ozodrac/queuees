@extends('layouts.app')

@section('title', '| Permissões do sistema')

@section('content')

@if(session('message'))
<div class="alert {{ session('message_class', 'alert-info') }} alert-dismissible fade show" role="alert">
  {{ session('message') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
<div class="container">
    <div class="row justify-content-center" id="app">
        <premissions-component></premissions-component>
    </div>
</div>
@endsection
