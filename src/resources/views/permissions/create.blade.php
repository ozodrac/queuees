@extends('layouts.app')

@section('title', '| Criar permissão')

@section('content')


@if(session('message'))
<div class="alert {{ session('message_class', 'alert-info') }} alert-dismissible fade show" role="alert">
  {{ session('message') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
@endif

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
              <i class='fa fa-plus'></i> Criar nova permissão
            </div>

            <div class="card-body">
                {{ Form::open(array('url' => 'permissions')) }}

                <div class="row">
                    <div class="form-group col-sm-4">
                      {{ Form::label('name', 'Name') }}
                      {{ Form::text('name', '', array('class' => 'form-control')) }}
                    </div>

                    <div class='form-group col-sm-4'>
                        {{ Form::label('roles', 'Regras') }}
                        <br />
                        @if(!$roles->isEmpty())
                            @foreach ($roles as $role) 
                                {{ Form::checkbox('roles[]',  $role->id ) }}
                                {{ Form::label($role->name, ucfirst($role->name)) }}<br>

                            @endforeach
                        @endif
                    </div>
                </div>

                {{ Form::submit('Criar', array('class' => 'btn btn-primary')) }}

                <a class="btn btn-danger" href="{{ route('permissions.index') }}">Voltar</a>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@endsection