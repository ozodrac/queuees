<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link active" href="/">
                    <i class="nav-icon icon-speedometer"></i> Inicio
                </a>
            </li>
            {{-- <li class="nav-title "> Configurações</li> --}}
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-shield-alt"></i> Segurança</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('auditorias.index') }}"><i class="fas fa-book"></i>
                            Auditorias</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('permissions.index') }}"><i class="icon-lock-open"></i>
                            Permissões</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('roles.index') }}"><i class="fa fa-user-shield"></i>
                            Perfil de Usuário</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('users.index')}}"><i class="fa fa-users"></i> Usuários</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('secretarias.index')}}">
                    <i class="fa fa-university"></i> Secretarias
                </a>
            </li>

            @if(auth()->user()->hasRole('Admin'))
            {{-- <li class="nav-item">
                    <a class="nav-link" href="{{route('endpoints.index')}}">
            <i class="icon-link"></i> Endpoints
            </a>
            </li> --}}
            @endif
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
