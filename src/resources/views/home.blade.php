@extends('layouts.app')

@section('content')

<div class="container">
    @if(session('message'))
    <div class="alert {{ session('message_class', 'alert-info') }} alert-dismissible fade show" role="alert">
        {{ session('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="row justify-content-center" id="app">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-list-alt"></i>Montar Prontuário Eletrônico do Cidadão</div>

                <div class="card-body">
                    {{ Form::open(array('url' => '/search')) }}

                    <div class="panel panel-default painel-comum">
                        <div class="panel-heading">
                            <h3 class="panel-title">Pesquisar por:</h3>
                        </div>
                        <div class="panel-body">
                            <div class="input-block" data-input-block--perline="2">
                                {{ Form::text('cpf', null, ['class' => 'form-control','placeholder'=>'Informe o CPF a ser pesquisado','required']) }}
                                {{ Form::label('cpf','<span class="resposta"></span>', [], false) }}
                            </div>
                        </div>
                        <br>
                        @foreach($secretarias as $secretaria)

                        <label class="switch switch-label switch-pill switch-success">
                            <input name="orgao[{{$secretaria->sigla}}]" class="switch-input" type="checkbox" checked="" value="{{$secretaria->endpoints()->first()->id}}">
                            {{--  <input name="orgao[{{$secretaria->sigla}}]" class="switch-input" type="checkbox" checked="" value="{{$secretaria->link}}">  --}}
                            <span class="switch-slider" data-checked="{{$secretaria->sigla}}"
                                data-unchecked="{{$secretaria->sigla}}"></span>
                        </label>
                        @endforeach
                        <br>
                    </div>
                    <br>
                    {{ Form::submit('Buscar Informações', array('class' => 'btn btn-success')) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
