@extends('layouts.app')

@section('title', '| Regras do sistema')

@section('content')

@if(session('message'))
<div class="alert {{ session('message_class', 'alert-info') }} alert-dismissible fade show" role="alert">
  {{ session('message') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif


<div class="row" id="app">
    <roles-component></roles-component>
    @php
     /*
    @endphp
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Perfil de Acesso
                <div class="card-actions">
                    {{--  <a href="{{ route('users.index') }}" class="btn btn-default pull-right"><small class="text-muted">Admins</small></a>  --}}
                    <a href="{{ route('permissions.index') }}" class="btn btn-default pull-right"><small class="text-muted">Permissões</small></a>
                    <a href="{{ route('roles.create') }}"><small class="text-muted">Criar nova regra</small></a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-responsive-sm table-striped">
                    <thead>
                        <tr>
                            <th>Regra</th>
                            <th>Permissão</th>
                            <th>Operações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($roles as $role)
                        <tr>
                            <td>{{ $role->name }}</td>
                            <td>{{ str_replace(array('[',']','"'),'', $role->permissions()->pluck('name')) }}</td>{{-- Retrieve array of permissions associated to a role and convert to string --}}
                            <td>
                            <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-primary btn-sm float-left mr-1"><i class="fa fa-pencil"></i></a>
                            {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                            {!! Form::button('<i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm')) !!}
                            {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
               {{ $roles->appends(Request::only(['name', 'email']))->links() }}
            </div>
        </div>
    </div>
    @php
        */
    @endphp
</div>
@endsection
