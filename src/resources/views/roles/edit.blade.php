@extends('layouts.app')

@section('title', '| Editar regra')

@section('content')


@if(session('message'))
<div class="alert {{ session('message_class', 'alert-info') }} alert-dismissible fade show" role="alert">
  {{ session('message') }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
@endif

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
              <i class='fa fa-key'></i>Editar perfil de acesso: {{$role->name}}
            </div>

            <div class="card-body">
                {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}

                <div class="row">
                    <div class="form-group col-sm-4">
                      {{ Form::label('name', 'Role Name') }}
                      {{ Form::text('name', null, array('class' => 'form-control')) }}
                    </div>

                    <div class='form-group col-sm-4'>
                        {{ Form::label('permissions', 'Permissões') }}
                        <br />
                        @foreach ($permissions as $permission)

                            {{Form::checkbox('permissions[]',  $permission->id, $role->permissions ) }}
                            {{Form::label($permission->name, ucfirst($permission->name)) }}<br>

                        @endforeach
                    </div>
                </div>

                {{ Form::submit('Atualizar', array('class' => 'btn btn-primary')) }}

                <a class="btn btn-danger" href="{{ route('roles.index') }}">Voltar</a>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@endsection
