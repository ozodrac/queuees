FROM php:7.3.9-apache

RUN a2enmod rewrite

# Install Composer
RUN apt-get -y update && \
    apt-get -y install curl nano git vim zip unzip libxml2-dev libmemcached-dev libmcrypt-dev zlib1g-dev && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN docker-php-ext-install mysqli mbstring pdo pdo_mysql tokenizer xml pcntl
RUN docker-php-ext-install sockets bcmath

# Configuration for Apache
COPY apache/000-default.conf /etc/apache2/sites-available
# # Config do Supervisor
# COPY supervisord/supervisord.conf /etc/supervisord.conf
# ENTRYPOINT [ "/user/bin/supervisord","-n","-c","/etc/supervisord.conf" ]


# APC
# RUN pear config-set php_ini /usr/local/etc/php/php.ini
# RUN pecl config-set php_ini /usr/local/etc/php/php.ini
RUN pecl channel-update pecl.php.net && pecl install memcached mcrypt-1.0.1 mongodb   && docker-php-ext-enable memcached mongodb 



# Clean after install
RUN apt-get autoremove -y && apt-get clean all

EXPOSE 80 443

# Change website folder rights and upload your website
RUN chown -R www-data:www-data /var/www/html
ADD ./src /var/www/html

# Change working directory
WORKDIR /var/www/html

# Create Laravel folders (mandatory)
RUN mkdir -p /var/www/html/bootstrap/cache
RUN mkdir -p /var/www/html/bootstrap/cache

RUN mkdir -p /var/www/html/storage/framework
RUN mkdir -p /var/www/html/storage/framework/sessions
RUN mkdir -p /var/www/html/storage/framework/views
RUN mkdir -p /var/www/html/storage/meta
RUN mkdir -p /var/www/html/storage/cache
RUN mkdir -p /var/www/html/public/storage/uploads/

# Change folder permission
RUN chmod -R 0777 /var/www/html/storage/
RUN chmod -R 0777 /var/www/html/public/storage/uploads/
# Laravel writing rights
RUN chgrp -R www-data /var/www/html/storage /var/www/html/bootstrap/cache
RUN chmod -R ug+rwx /var/www/html/storage /var/www/html/bootstrap/cache

# Install and update laravel (rebuild into vendor folder)


RUN service apache2 restart